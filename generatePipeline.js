const { writeFileSync } = require('fs');
const { execSync } = require('child_process');

const successTag = 'pipeline-success';
const tagRemote = 'tag-origin';

const pipelineFile = process.argv[2];
const tags = process.argv[3];
const tagTemplate = !tags
  ? ''
  : `  tags:\n${tags
      .split(',')
      .map((t) => `    - ${t}`)
      .join('\n')}`;

if (!pipelineFile) {
  throw new Error('No pipeline file given!');
}

function getLastSuccessfulCommit() {
  try {
    return execSync(`git rev-list -n 1 ${successTag}`).toString().trim();
  } catch (error) {
    console.warn(
      `WARNING: Could not get last successful commit from tag ${successTag}!`
    );
    console.warn(error);
  }
}

function getProjectsFromAffected() {
  if (process.env.CI_COMMIT_BEFORE_SHA === '0'.repeat(40))
    process.env.CI_COMMIT_BEFORE_SHA = 'origin/master';
  const base =
    process.env.NX_BASE_SHA ||
    getLastSuccessfulCommit() ||
    process.env.CI_COMMIT_BEFORE_SHA ||
    'origin/master~1';
  console.log(`Calculating diff from base ${base}`);
  return execSync(
    `nx print-affected --target=build --base=${base} --select="tasks.target.project" | sed -e 's#\\s\\+##g'`
  )
    .toString()
    .trim();
}

const projectsStr =
  process.env.PROJECTS?.trim() || getProjectsFromAffected() || '';
const projects = projectsStr.split(',');
if (projectsStr) console.log(`Run pipeline for projects ${projectsStr}`);

const stages = [];

if (projectsStr) {
  stages.push('build', 'docker', 'deploy');
}

stages.push('done');

const jobs = [
  {
    name: 'dockerizer',
    stage: 'docker',
    contents: `
  image: docker
  services:
    - docker:dind
  dependencies:
${projects.map((project) => `    - build_${project}`).join('\n')}
  script:
    - echo $DOCKER_HUB_ACCESS_TOKEN | docker login -u $DOCKER_HUB_USERNAME --password-stdin
    - cat $GCP_SERVICE_KEY | docker login -u _json_key --password-stdin https://$GCP_REGISTRY_HOST
    - apk add --update npm git
    - npm i --cache .npm --prefer-offline -g @nrwl/cli@12.7.1
    - npm i --cache .npm --prefer-offline --no-package-lock --no-save --unsafe-perm --ignore-scripts --production
    - npm i --cache .npm --prefer-offline --no-package-lock --no-save --unsafe-perm --ignore-scripts @nx/workspace@12.7.1 @types/node@14.14.33 typescript@~4.3.5
    - npm run build:executors
    - apk add zip
    - zip node_modules.zip -q -r node_modules
    - npx nx run-many --projects=${projectsStr} --target=docker -- --build --gcpProject=$GCP_PROJECT --remoteHost=$REMOTE_REGISTRY_HOST --gcpHost=$GCP_REGISTRY_HOST
    - npx nx run-many --projects=${projectsStr} --target=docker --parallel --maxParallel=100 -- --push --gcpProject=$GCP_PROJECT --remoteHost=$REMOTE_REGISTRY_HOST --gcpHost=$GCP_REGISTRY_HOST
  artifacts:
    paths:
      - digests.env
    `,
  },
  {
    name: 'deployer',
    stage: 'deploy',
    contents: `
  image: google/cloud-sdk:alpine
  dependencies:
${projects.map((project) => `    - build_${project}`).join('\n')}
    - dockerizer
  script:
    - gcloud auth activate-service-account --project $GCP_PROJECT --key-file $GCP_SERVICE_KEY
    - apk add --update npm git
    - npm i --cache .npm --prefer-offline -g @nrwl/cli@12.7.1
    - npm i --cache .npm --prefer-offline --no-package-lock --no-save --unsafe-perm --ignore-scripts @nx/workspace@12.7.1 @types/node@14.14.33 typescript@~4.3.5
    - npm run build:executors
    - npx nx run-many --projects=${projectsStr} --target=deploy --parallel --maxParallel=100 -- --gcpProject=$GCP_PROJECT --source=$REMOTE_REGISTRY_HOST --gcpHost=$GCP_REGISTRY_HOST --serviceAcc=$GCP_SERVICE_ACCOUNT --region=$GCP_REGION
    `,
  },
  {
    name: 'done',
    stage: 'done',
    contents: `
  image: alpine
  script:
    - apk add --update git
    - git remote add ${tagRemote} https://oauth2:$GITLAB_TOKEN@gitlab.com/Whatshiywl/bubaxi.git
    - git tag -d ${successTag} || echo No local tag to delete
    - git push ${tagRemote} --delete ${successTag} || echo No remote tag to delete
    - git tag ${successTag} $CI_COMMIT_SHA
    - git push ${tagRemote} ${successTag}
    - echo Done!${
      projectsStr
        ? `
  dependencies:
    - deployer`
        : ''
    }
    `,
  },
];

projects.map((project) => {
  jobs.push({
    name: `build_${project}`,
    stage: 'build',
    contents: `
  image: node:16-alpine
  script:
    - npm i --cache .npm --prefer-offline -g @nrwl/cli@12.7.1
    - npm i --cache .npm --prefer-offline --no-package-lock --no-save --unsafe-perm --ignore-scripts
    - npx nx run-many --projects=${project} --target=build --prod --parallel --maxParallel=100
  artifacts:
    paths:
      - dist
    `,
  });
});

const toWrite = `
stages:
${stages.map((stage) => `  - ${stage}`).join('\n')}

# Cache modules in between jobs
cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .npm/

${stages
  .map((stage) =>
    jobs
      .filter((job) => job.stage === stage)
      .map(
        (job) => `
${job.name}:
  stage: ${stage}
${tagTemplate}
${job.contents}`
      )
      .join('\n')
  )
  .join('\n')}`;

console.info(`${pipelineFile}:\n${toWrite}`);

writeFileSync(pipelineFile, toWrite);
